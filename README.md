# Automating React Native builds and deployments with fastlane

## How fastlane has helped us

Over the past couple of years we have been creating more and more apps using React Native and one of the things which consistantly causes issues is the process of sharing certificates to codesign on multiple development devices. In some instances it has led to creating multiple certificates for projects causing conflicts when you try to run builds. This can be a massive time sink, and so we decided to invest some time to look at some of the tools out there for certificate managment. Fatslane not only provided the tools we needed to manage codesigning certificates, it also provided extra tools to help streamline how we safely deploy Beta and production builds of our apps. With some really easy to write custom ruby gem scripts, we can now install the relevant certificates, build and deploy our apps to both app stores as test and productions versions in a matter of seconds! A vast improvement of what was on occasion taking several ours to roll out. Not only is it quicker, but as everything is now automated the process is much stricter and safer leaving alot less room for human error in the build process! Below is a quick tutorial for getting setup with fastlane in a React Native project.

## Setup Fastfile and Appfile

These are two gem files where we can define variables and "lanes" (sequences of functions) which we can then use as custom commands to automate certain processed. To make things easier we can set up all our fastlane enviroment variables in the Appfile first so that commands don't keep asking us for the information. At the route of the directory we will have a fastlane folder and then the Appfile within that looking something like this:

```ruby
# IOS config
app_identifier "com.app.identifier" # The bundle identifier of your app
apple_id "apple@id.co.uk"  # Your Apple email address
team_name "TEAM NAME" # Team name for your itunes connect account
team_id "TEAMID" # Team ID for your itunes connect account - found at https://developer.apple.com/account/#/membership/S7R8RC82YA
itc_team_name "TEAM NAME" # Team name for your itunes connect account
itc_team_id "TEAM ID"  # Team ID for your itunes connect account

# Android config
json_key_file "./path-to-json-key.json" # JSON key generated for the android play store account instructions for creating at: https://docs.fastlane.tools/getting-started/android/setup/#collect-your-google-credentials
package_name "com.android.package.id" # Application id for the android app found in build.gradle file
```

## Certificate creation and sharing using match

Match is a one of the fastlane commands which is specifically used for IOS certificate creation and management. Android apps are codesigned automatically using a keystore file which we won't go through here but details can be found at https://developer.android.com/studio/publish/app-signing. To get started we need to create a new private git repo (name something like "app-name-certificates") which will store our certificate files for us to make them easily shared between developer devices. Then from the route of the project run `fastlane match init`. This is going to prompt you for the url of the git repository we just created. Once the command has run you will have a Matchfile, which can be moved to the previously created fastlane directory. In this file add the app_identifier and username so that it doesnt prompt us for every command that follows. All we have to do now is create the certificates using:

`$ fastlane match development`
and
`$ fastlane match appstore`

Follow the command prompts (**NOTE**: save the password you use for these commands somewhere safe) and thats it! Fastlane has now created development and distribution certificates and provisioning profiles for the app. To get the certificates onto another development machine you simply have to run:
`$ fastlane match development --readonly`
and
`$ fastlane match appstore --readonly`
and they are ready go! To make this all slightly easier and more inline with React Natives development setup though we can use a "lane" to automate this in the Fastfile like so:

```ruby
platform :ios do
 desc 'Fetch certificates and provisioning profiles'
 lane :certificates do
   match(app_identifier: 'uk.co.gravitywell.gaku.app', type: 'development', readonly: true)
   match(app_identifier: 'uk.co.gravitywell.gaku.app', type: 'appstore', readonly: true)
 end
end
```

This means we can run `$ fastlane ios certificates` and everything we need is setup. But let's go one further and move this to the package.json file as a script:

```json
  "scripts": {
    "start": "node node_modules/react-native/local-cli/cli.js start",
    "test": "jest",
    "ios:certificates": "fastlane ios certificates"
  },
```

Now `$ npm run certificates` sets up the project ready for development and roduction builds!

## Automating builds and beta releases

Now all the certificates are setup and our app build successfully in android studio and xcode, we want to be able to run one command to build and deploy test versions of our app to the relevant app stores.

### IOS

Once again go into the Fastfile and add the following snippet within the `platform :ios do` scope.

```ruby
  desc 'Build the iOS application.'
  private_lane :build do
    certificates
    increment_build_number(xcodeproj: './ios/clientApp.xcodeproj')
    commit_version_bump(message: 'Bump build', xcodeproj: './ios/clientApp.xcodeproj')
    gym(scheme: 'clientApp', project: './ios/clientApp.xcodeproj')
  end

  desc 'Ship to Testflight.'
  lane :beta do
    build
    upload_to_testflight(skip_submission: true)
    add_git_tag(tag: "@Beta-" + get_version_number().to_s + "-" + get_build_number().to_s )
    push_to_git_remote
  end
```

The first command `private_lane :build` does the following:

- Get the relevant certificates
- Increment the build nmber
- commit this increment to git
- build the app using the 'clientApp`

The second public lane does the following:

- Run the `private_lane :build` we created created
- upload the build files to itunes connect testflight
- when successful tag the project as a beta release with the build version and number
- Push all the updates to git remote repository

Once again we can add the following `ios:beta` to the package.json file:

```json
"scripts": {
    "start": "node node_modules/react-native/local-cli/cli.js start",
    "test": "jest",
    "ios:certificates": "fastlane ios certificates",
    "ios:beta": "fastlane ios beta",
  },
```

Now simply run `$ npm run ios:beta` and the project is going to be submitted to testflight for testers to review in one simple command!

## Continuing with fastlane

From this point it is really easy to continue extending fastlane to best suit your teams development practices, an example of our Fastfile can be viewed [here](https://bitbucket.org/georgeevans0106/fastlane-demo), with a load of other useful "lanes" for automating deploys and managing certificates. The great thing about fastlane though is that you can completely determine the process you want your app to go through to get to release. There's loads more fastlane can do with the right config; you can autogenerate screenshots of your app for the appstore release; integrate notifications to slack to keep your team informaed about deployments and releases; integrate CI tools such as Travis or Jenkins. To see some other useful commands you can use to improve your deployment process visit their great [documentation site](https://docs.fastlane.tools/).
